<?php

// Nota: Arquivo renomeado de sql.php para Sql.php

class Sql extends PDO{
    private $conn;
    
    public function __construct()
    {
        $this->conn = new PDO(
            "mysql:dbname=dpphp7;host=localhost",
            "bran01",
            "password",
            [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES'UTF8'"
            ]);
    }
    
    private function setParams(&$statement,$parameters= array())
    {
        foreach($parameters as $key =>$value){
            $this->setParam($statement,$key,$value);
        }
    }
    
    private function setParam(&$statement, $key,$value)
    {
        $statement->bindParam($key,$value);

    }
    
    public function query($rawQuery, $params = array())
    {
        if (empty($params)) {   
            return $this->conn->query($rawQuery);
        } else {
            $stmt=$this->conn->prepare($rawQuery);
            //$this->setParams($stmt, $params);
            // var_dump($params);
            $stmt->execute($params);
            return $stmt;
        }
    
        // $stmt=$this->conn->prepare($rawQuery);
        // $this->setParams($stmt,$params);
        // $stmt->execute($params);
        // return $stmt;
    }

    
    public function select($rawQuery,$params= array()):array
    {
        $stmt=$this->query($rawQuery,$params);

        if(!$stmt || !$data=$stmt->fetchAll(PDO::FETCH_ASSOC)){
            return [];
        }
        return ($stmt->errorCode()!='0000')? [] : $data;
    }

    
    public function show($rawQuery,$params= array()):array
    {
        $stmt=$this->query($rawQuery,$params);

        if(!$stmt || !$data = $stmt->fetch(PDO::FETCH_ASSOC)){
            return [];
        }

        return ($stmt->errorCode()!='0000')? [] : $data;
    }
}
// $conn= new PDO("mysql:host=localhost;dbname=dpphp7","root", "");
// $conn->beginTransaction();
// $stmt=$conn->prepare("DELETE tb_usuarios WHERE idusuario=?");
// $id=2;
// $stmt->execute(array($id));
// $conn->commit();

//$stmt=$conn->prepare("DELETE FROM tb_usuarios WHERE idusuario = :ID");
//$id=1;
//$stmt->bindParam(":ID",$id);
//$stmt->execute();
// echo "delete OK!";
