create database dpphp7;

use dpphp7;

CREATE TABLE tb_usuarios(
idusuario INT NOT NULL auto_increment PRIMARY KEY,
deslogin VARCHAR(64) NOT NULL,
dessenha VARCHAR(256),
DTCADASTRO timestamp not null default current_timestamp()
);

INSERT INTO tb_usuarios (deslogin, dessenha) VALUES ('root', '1223');
INSERT INTO tb_usuarios (deslogin, dessenha) VALUES ('root2', '12233');

select * from tb_usuarios;
update tb_usuarios set dessenha='123456' where idusuario=1;

alter table tb_usuarios change columns DTCADASTRO timestamp dtcadastro ;
