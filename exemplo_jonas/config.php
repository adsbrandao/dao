<?php
spl_autoload_register(function($class_name){
    $filename = str_replace("\\", DIRECTORY_SEPARATOR, $class_name).'.php';
    $filename1 = "class".DIRECTORY_SEPARATOR.$filename;
    if(file_exists(($filename))){
        require_once($filename);
    } elseif(file_exists(($filename1))){
        require_once($filename1);
    }
});


use Database\Connect;

Connect::setConnection(
    'localhost',
    'dpphp7',
    'bran01',
    'password',
    'mysql',
    [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES'UTF8'"
    ]
);
