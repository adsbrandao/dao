<?php

// Para facilitar debug:
ini_set('display_errors', true);
error_reporting(E_ALL);

require_once("config.php");
//$root = new Usuario();
//$root->loadbyId(9);
//echo $root;

use Usuario;

$usuario = new Usuario();
echo "<h1>Mostrar usuario 1</h1>";
$res = $usuario->find(1);
echo $res->deslogin;

$usuario->select("*");
echo "<h1>Mostrar lista (toString)</h1>";
foreach ($usuario->getAll() as $item) {
    echo "<br>" . $item;
}

$search = Usuario::search("jo");
echo "<h1>Mostrar lista procurar por jo</h1>";
foreach ($search as $item) {
    echo "<br>" . $item;
}

try {
    echo "<h1>Mostrar login e mostrar (toString)</h1>";
    $res = $usuario->login("jonas","123456");
    echo $res;
} catch (\Exception $e) {
    echo 'ERROR: '.$e->getMessage();
}
try {
    echo "<h1>Mostrar login 2 e mostrar (toString)</h1>";
    $res = $usuario->login("jonas","12345");
    echo $res;
} catch (\Exception $e) {
    echo 'ERROR: '.$e->getMessage();
}


echo "<h1>Pegar id 7 e Deletar (toString)</h1>";
$usuario->delete(7);

echo "<h1>Fazer update 6</h1>";
$usuario->update(['deslogin' => "professor", 'dessenha' => "1231237832904"], 6);

//Comentario
//$sql=new Sql();
//$usuarios=$sql->select("SELECT * FROM tb_usuarios");
//echo json_encode($usuarios);
//#$sql->select("SELECT * FROM tb_usuarios");
//fim

//$lista=Usuario::getList();
//echo json_encode();

//$search =Usuario::search("jo");
//echo json_encode($search);

//$usuario= new Usuario();
//$usuario->login("root","");
//echo $usuario;

// $aluno=new Usuario("aluno", "@lun0");
// $aluno->setDeslogin("aluno");
// $aluno->setDessenha("@alun0");
// $aluno->insert();
// echo $aluno;
