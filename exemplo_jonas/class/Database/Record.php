<?php namespace Database;

class Record {
    /** @var Model */
    private $_model;
    private $_data;
    public function __construct (Model $model, $data = [])
    {
        $this->_model = $model;
        $this->_data = $data;
    }

    public function __get ($name)
    {
        if (isset($this->_data[$name])) {
            return $this->_data[$name];
        }

        throw new \Exception("Campo '{$name}' não definido", 1);
    }

    public function __set ($name, $value)
    {
        $this->_data[$name] = $value;
    }

    public function __toString ()
    {
        return json_encode($this->_data, JSON_UNESCAPED_UNICODE);
    }

    public function getData ()
    {
        return $this->_data;
    }

    public function save ()
    {
        $primaryKey = $this->_model->getPrimaryKey();

        if (isset($this->_data[$primaryKey]) && !is_null($this->_data[$primaryKey])) {
            // Update
            $this->_model->update($this->_data, $this->_data[$primaryKey]);
        } else {
            $id = $this->_model->insert($this->_data);
            $this->_data[$primaryKey] = $id;
        }
    }

    public function delete ()
    {
        if (isset($this->_data[$primaryKey]) && !is_null($this->_data[$primaryKey])) {
            $this->_model->delete($this->_data[$primaryKey]);
            return true;
        }
        return false;
    }
}