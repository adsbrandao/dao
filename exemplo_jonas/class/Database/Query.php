<?php namespace Database;

class Query {
    public function exec ($rawQuery, $params = []): \PDOStatement
    {
        $conn = Connect::getConnection();
        
        if (empty($params)) {   
            $stmt = $conn->query($rawQuery);
        } else {
            $stmt = $conn->prepare($rawQuery);
            $stmt->execute($params);
        }

        if(!$stmt){
            list($code, $codeDriver, $message) = $stmt->errorInfo();
            throw new \Exception("Error: #{$codeDriver} - ".$message, $code);
        }

        return $stmt;
    }

    public function get ($rawQuery, $params = [], $fetch_style = \PDO::FETCH_ASSOC, $cursor_orientation = 0, $cursor_offset = 0)
    {
        $stmt = $this->exec($rawQuery, $params);

        if ($stmt->rowCount() == 0) {
            throw new \Exception("Nenhum registro encontrado", 1);
        }

        return $stmt->fetch($fetch_style, $cursor_orientation, $cursor_offset);
    }

    public function getAll ($rawQuery, $params = [], $fetch_style = \PDO::FETCH_ASSOC, $cursor_orientation = 0, $cursor_offset = 0):array
    {
        $stmt = $this->exec($rawQuery, $params);

        if ($stmt->rowCount() == 0) {
            return [];
        }

        return $stmt->fetchAll($fetch_style, $cursor_orientation, $cursor_offset);

    }
}