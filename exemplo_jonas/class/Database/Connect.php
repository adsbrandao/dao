<?php namespace Database;

class Connect {
    static $conn;
    static $dataConn;
    
    /**
     * Define dados de conexao com banco de dados
     *
     * @param String $host
     * @param String $dbname
     * @param Strin $username default: 'root'
     * @param String $password Default: ''
     * @param String $driver Default: 'mysql'
     * @param Array $options Defualt: []
     * @return void
     */
    static public function setConnection ($host, $dbname, $username = 'root', $password = '',$driver = 'mysql', $options = [])
    {
        self::$dataConn = (object)[
            'host' => $host,
            'dbname' => $dbname,
            'username' => $username,
            'password' => $password,
            'driver' => $driver,
            'options' => $options
        ];
    }

    /**
     * Pega conexao ao banco de dados
     * 
     * @throws \Exception
     *
     * @return \PDO
     */
    static public function getConnection (): \PDO
    {
        if (is_null(self::$conn)) {
            if (is_null(self::$dataConn)) {
                throw new \Exception("Conexão não definida", 1);
            }
            self::$conn = new \PDO(
                self::$dataConn->driver.":dbname=".self::$dataConn->dbname.";host=".self::$dataConn->host."",
                self::$dataConn->username,
                self::$dataConn->password,
                self::$dataConn->options
            );
        }

        return self::$conn;
    }
}