<?php namespace Database;


class QueryBuilder {
    private $_table;
    private $_primaryKey = 'id';
    private $_select = '*';
    private $_joins = [];
    private $_where;
    private $_orderBy;
    private $_limit = 0;
    private $_start = 0;
    private $_params = [];
    private $_lastQuery;

    public function getLastQuery (): String
    {
        if (is_null($this->_lastQuery)) {
            return $this->getRawQuery();
        }
        return $this->_lastQuery;
    }

    public function setTable($table, $primaryKey = 'id')
    {
        $this->_table = $table;
        $this->_primaryKey = $primaryKey;
    }

    public function select($select)
    {
        $this->_select = $select;
    }

    public function join($table, $on, $joinType = 'inner')
    {
        $joinType = strtoupper($joinType);
        if (!in_array($joinType, ['INNER', 'LEFT', 'RIGHT'])) {
            $joinType = 'INNER';
        }
        $this->_joins[] = $joinType . " JOIN " . $table . " ON ".$on;
    }

    public function where($where, $params = [])
    {
        if (is_null($this->_where)) {
            $this->_where = "";
        } else {
            $this->_where .= " AND ";
        }
        $this->_where .= $where;
        $this->_params = array_merge($this->_params, $params);
    }

    public function whereOr ($where, $param = [])
    {
        if (is_null($this->_where)) {
            $this->_where = "";
        } else {
            $this->_where .= " OR ";
        }
        $this->_where .= $where;
        $this->_params = array_merge($this->_params, $params);
    }

    public function orderBy ($order) 
    {
        $this->_orderBy = $order;
    }

    public function limit ($limit) 
    {
        $this->_limit = $limit;
    }

    public function start ($start) 
    {
        $this->_start = $start;
    }

    public function find ($id, $fetch_style = \PDO::FETCH_ASSOC)
    {        
        $this->_where = "{$this->_primaryKey} = ?";
        $this->_params = [$id];
        
        return $this->get($fetch_style);
    }
    
    public function get ($fetch_style = \PDO::FETCH_ASSOC)
    {
        $this->_limit = 1;

        $query = new Query();
        $stmt = $query->exec($this->getRawQuery(), $this->_params);

        if (!$stmt) return false;

        return $stmt->fetch($fetch_style);
    }

    public function getAll ($fetch_style = \PDO::FETCH_ASSOC)
    {
        $query = new Query();
        $stmt = $query->exec($this->getRawQuery(), $this->_params);

        if (!$stmt) return [];

        return $stmt->fetchAll($fetch_style);
    }

    private function getRawQuery (): String
    {
        $params = [];
        $sql = "SELECT ". $this->_select . " FROM ".$this->_table;

        $sql .= implode(" ", $this->_joins);
        
        $where = "";
        if (!is_null($this->_where)) {
            $where = " WHERE {$this->_where}";
        }

        if (!is_null($this-> _orderBy)) {
            $sql .= " ORDER BY {$this-> _orderBy}";
        }

        if (!is_null($this->_limit) && $this->_limit > 0) {
            if (is_null($this->_start)) {
                $sql .= " LIMIT {$this->_limit}";
            } else {
                $sql .= " LIMIT {$this->_start}, {$this->_limit}";
            }
        }

        $this->_lastQuery = $sql;
        return $sql;
    }

    public function delete ($id = null)
    {
        $params = [];
        $sql = "DELETE FROM ". $this->_table;
        
        $where = "";
        if (is_null ($id)) {
            if (!is_null($this->_where)) {
                $where = "{$this->_where}";
                $params = array_merge($params, $this->_params);
            }
        } else {
            $where = "{$this->_primaryKey} = ?";
            $params[] = $id;
            $this->_params = [$id];
        }

        if ($where != '') {
            $sql .= " WHERE {$where}";
        }
        $this->_where = $where;

        if (!is_null($this-> _orderBy)) {
            $sql .= " ORDER BY {$this-> _orderBy}";
        }

        if (!is_null($this->_limit) && $this->_limit > 0) {
            if (is_null($this->_start)) {
                $sql .= " LIMIT {$this->_limit}";
            } else {
                $sql .= " LIMIT {$this->_start}, {$this->_limit}";
            }
        }

        $this->_lastQuery = $sql;
        $query = new Query();
        return $query->exec($sql, $params);
    }

    public function update ($data, $id = null): \PDOStatement
    {
        $params = [];
        $sql = "UPDATE ". $this->_table . " SET ";
        $sep = "";
        foreach ($data as $campo=>$valor) {
            $sql .= "{$sep}{$campo} = ?";
            $sep = ", ";
            $params[] = $valor;
        }

        $where = "";
        if (is_null ($id)) {
            if (!is_null($this->_where)) {
                $where = "{$this->_where}";
                $params = array_merge($params, $this->_params);
            }
        } else {
            $where = "{$this->_primaryKey} = ?";
            $params[] = $id;
            $this->_params = [$id];
        }

        if ($where != '') {
            $sql .= " WHERE {$where}";
        }
        $this->_where = $where;

        if (!is_null($this-> _orderBy)) {
            $sql .= " ORDER BY {$this-> _orderBy}";
        }

        if (!is_null($this->_limit) && $this->_limit > 0) {
            if (is_null($this->_start)) {
                $sql .= " LIMIT {$this->_limit}";
            } else {
                $sql .= " LIMIT {$this->_start}, {$this->_limit}";
            }
        }

        $this->_lastQuery = $sql;
        $query = new Query();
        return $query->exec($sql, $params);
    }

    public function insert ($data)
    {
        $params = [];
        $sql = "INSERT INTO ". $this->_table;
        $campos = "";
        $valores = "";
        $sep = "";
        foreach ($data as $campo=>$valor) {
            $campos .= "{$sep}{$campo}";
            $valores .= "{$sep}?";
            $sep = ", ";
            $params[] = $valor;
        }

        $this->_lastQuery = $sql;
        $query = new Query();
        $query->exec($sql, $params);

        $id = Connect::getConnection()->lastInsertId();

        $this->_where = "{$this->_primaryKey} = ?";
        $this->_params = [$id];

        return $id;
    }
}