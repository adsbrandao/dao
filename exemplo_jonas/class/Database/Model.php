<?php namespace Database;

class Model {
    protected $table;
    protected $primaryKey;

    /** @var QueryBuilder */
    private $_queryBuilder;

    public function __construct ()
    {
        $this->_queryBuilder = new QueryBuilder();
        $this->_queryBuilder->setTable($this->table, $this->primaryKey);
    }

    public function getPrimaryKey ()
    {
        return $this->primaryKey;
    }

    public function getQuery (): String
    {
        return $this->_queryBuilder->getLastQuery();
    }

    public function getQueryBuilder (): QueryBuilder
    {
        return $this->_queryBuilder;
    }
    
    public function select($select = '*')
    {
        $this->_queryBuilder->select($select);
    }
    
    public function join($table, $on, $joinType = 'inner')
    {
        $this->_queryBuilder->join($table, $on, $joinType);
    }
    
    public function where($where, $params = [])
    {
        $this->_queryBuilder->where($where, $params);
    }
    
    public function whereOr ($where, $param = [])
    {
        $this->_queryBuilder->whereOr($where, $param);
    }
    
    public function orderBy ($order) 
    {
        $this->_queryBuilder->orderBy($order) ;
    }
    
    public function limit ($limit) 
    {
        $this->_queryBuilder->limit($limit) ;
    }
    
    public function start ($start) 
    {
        $this->_queryBuilder->start($start) ;
    }
    
    public function find($id)
    {
        $data = $this->_queryBuilder->find($id, \PDO::FETCH_ASSOC);
        return new Record($this, $data);
    }
    
    /**
     * Pega primeiro resultado
     *
     * @return Record
     */
    public function get ()
    {
        $data = $this->_queryBuilder->get(\PDO::FETCH_ASSOC);
        
        if (!$data) return false;
        
        return new Record($this, $data);
    }
    
    public function getAll ()
    {
        $data = $this->_queryBuilder->getAll(\PDO::FETCH_ASSOC);

        $data = array_map(function ($item) {
            return new Record($this, $item);
        }, $data);
        
        return $data;
    }
    
    public function delete ($id = null)
    {
        return $this->_queryBuilder->delete($id);
    }
    
    public function update ($data, $id = null): \PDOStatement
    {
        return $this->_queryBuilder->update($data, $id);
    }
    
    public function insert ($data)
    {
        return $this->_queryBuilder->insert($data);
    }
}