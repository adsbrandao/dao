<?php

use Database\Model;

class Usuario extends Model {
    protected $table = "tb_usuarios";
    protected $primaryKey = "idusuario";

    public static function search($search)
    {
        $model = new Usuario();
        $model->where('deslogin LIKE :SEARCH', [':SEARCH'=>"%".$search."%"]);
        $model->orderBy('deslogin');
        return $model->getAll();
    }
    
    public function login($login, $password)
    {
        $this->select('*');
        $this->where('deslogin LIKE :LOGIN', [':LOGIN'=> $login]);
        $res = $this->get();

        if (!$res || $res->dessenha != $password) {
            throw new \Exception("Usuário ou senha inválidos.");
        }

        return $res;
    }
}