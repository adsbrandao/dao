<?php

use Database\Model;

// Exemplo 2
class Usuario2 extends Model {
    protected $table = "tb_usuarios";
    protected $primaryKey = "idusuario";

    private $idusuario;
    private $deslogin;
    private $dessenha;
    private $dtcadastro;

    // getters
    public function getIdusuario ()
    {
        return $this->idusuario;
    }

    public function getDeslogin ()
    {
        return $this->deslogin;
    }

    public function getDessenha ()
    {
        return $this->dessenha;
    }

    public function getDtcadastro ()
    {
        return $this->dtcadastro;
    }

    public function getDtcadastroDateFormat ($format = 'd/m/Y H:i:s')
    {
        $date = date($format, strtotime($this->dtcadastro));
    }

    // setters


    /**
     * @return Usuario2
     */
    public function setDeslogin ($value)
    {
        $this->deslogin = $value;
        return $this;
    }


    /**
     * @return Usuario2
     */
    public function setDessenha ($value)
    {
        $this->dessenha = $value;
        return $this;
    }

    public function __serialize ()
    {
        $data = [
            'idusuario' => $this->idusuario,
            'deslogin' => $this->deslogin,
            'dessenha' => $this->dessenha,
            'dtcadastro' => $this->dtcadastro
        ];

        return $data;
    }

    public function __unserialize ($data)
    {
        $this->idusuario = $data['idusuario'];
        $this->deslogin = $data['deslogin'];
        $this->dessenha = $data['dessenha'];
        $this->dtcadastro = $data['dtcadastro'];
    }

    /**
     * @return Usuario2
     */
    public function save ()
    {
        if (is_null($this->idusuario)) {
            $data = $this->serialize();

            unset($data['idusuario'], $data['dtcadastro']);
            $id = $this->getQueryBuilder()->insert($data);

            $novo = $this->find($id);
            $this->unserialize($novo->serialize());

            return $this;
        } else {
            $data = $this->unserialize();

            unset($data['idusuario'], $data['dtcadastro']);
            $this->getQueryBuilder()->update($data,$this->idusuario);

            $novo = $this->find($this->idusuario);
            $this->unserialize($novo->serialize());

            return $this;
        }
    }

    /**
     * @return Usuario2
     */
    public function find($id)
    {
        $data = $this->getQueryBuilder()->find($id, \PDO::FETCH_ASSOC);

        $this->unserialize($data);
        
        return $this;
    }
    
    /**
     * Pega primeiro resultado
     *
     * @return Usuario2
     */
    public function get ()
    {
        $data = $this->getQueryBuilder()->get(\PDO::FETCH_ASSOC);
        
        if (!$data) return false;

        $this->unserialize($data);
        
        return $this;
    }
    
    public function getAll ()
    {
        $data = $this->getQueryBuilder()->getAll(\PDO::FETCH_ASSOC);

        $data = array_map(function ($item) {
            $c = new self();
            $c->unserialize($item);
            return $c;
        }, $data);
        
        return $data;
    }
    
    public function delete ($id = null)
    {
        return $this->getQueryBuilder()->delete($id);
    }

    public static function search($search)
    {
        $model = new Usuario();
        $model->where('deslogin LIKE :SEARCH', [':SEARCH'=>"%".$search."%"]);
        $model->orderBy('deslogin');
        return $model->getAll();
    }
    
    public function login($login, $password)
    {
        $this->select('*');
        $this->where('deslogin LIKE :LOGIN', [':LOGIN'=> $login]);
        $res = $this->get();

        if (!$res || $res->dessenha != $password) {
            throw new \Exception("Usuário ou senha inválidos.");
        }

        return $res;
    }
}


// Exemplo:

$usuario = new Usuario2();

$res1 = $usuario->find(1);

echo $res1->getDeslogin(); // mostra o login do usuario 1

$res1->setDeslogin("Marcio")->save(); // altera o login do usuario 1

// Criar um usuario:
$usuario = new Usuario2();

$usuario->setDeslogin("Paulo")
    ->setDessenha("123456")
    ->save();

echo $usuario->getIdusuario(); // Mostra o id do novo usuario Paulo
echo $usuario->getDtcadastroDateFormat('d/m/Y H:i:s'); // Mostra a data de cadastro do usuario Paulo