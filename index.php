<?php

// Para facilitar debug:
ini_set('display_errors', true);
error_reporting(E_ALL);

require_once("config.php");
//$root = new Usuario();
//$root->loadbyId(9);
//echo $root;

$usuario= new Usuario();
$usuario->loadbyId(1);
echo "<h1>Mostrar usuario 1</h1>";
echo $usuario;


$sql=new Sql();
$usuario2= new Usuario();
$usuarios2=$sql->select("SELECT * FROM tb_usuarios");
echo "<h1>Mostrar lista (toString)</h1>";
echo $usuario2;

$lista=Usuario::getList();
echo "<h1>Mostrar lista (JsonEncode)</h1>";
echo json_encode($lista);

$search =Usuario::search("jo");
echo "<h1>Mostrar lista procurar por jo</h1>";
echo json_encode($search);

try {
    echo "<h1>Mostrar login e mostrar (toString)</h1>";
    $usuario4= new Usuario();
    $usuario4->login("jonas","123456");
    echo $usuario4;
} catch (\Exception $e) {
    echo 'ERROR: '.$e->getMessage();
}
try {
    echo "<h1>Mostrar login 2 e mostrar (toString)</h1>";
    $usuario4= new Usuario();
    $usuario4->login("jonas","12345");
    echo $usuario4;
} catch (\Exception $e) {
    echo 'ERROR: '.$e->getMessage();
}


echo "<h1>Pegar id 7 e Deletar (toString)</h1>";
$usuario5= new Usuario();
$usuario5->loadById(7); // Funcao nao estava igual (case sensetive)
$usuario5->delete();
echo $usuario5;

echo "<h1>Fazer update 6</h1>";
$usuario6= new Usuario();
$usuario6->loadById(6);
$usuario6->update("professor","1231237832904");
echo $usuario6;

//Comentario
//$sql=new Sql();
//$usuarios=$sql->select("SELECT * FROM tb_usuarios");
//echo json_encode($usuarios);
//#$sql->select("SELECT * FROM tb_usuarios");
//fim

//$lista=Usuario::getList();
//echo json_encode();

//$search =Usuario::search("jo");
//echo json_encode($search);

//$usuario= new Usuario();
//$usuario->login("root","");
//echo $usuario;

// $aluno=new Usuario("aluno", "@lun0");
// $aluno->setDeslogin("aluno");
// $aluno->setDessenha("@alun0");
// $aluno->insert();
// echo $aluno;
